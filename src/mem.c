#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) {
    return size_max( round_pages( query ), REGION_MIN_SIZE );
}

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    size_t real_size = region_actual_size(size_from_capacity((block_capacity){query}).bytes);
    void* region_ptr = map_pages(addr, real_size, MAP_FIXED_NOREPLACE);
    if (region_ptr == MAP_FAILED){
        region_ptr = map_pages(addr, real_size, 0);
        if(region_ptr == MAP_FAILED){
            return REGION_INVALID;
        }
        struct region region = { .addr = region_ptr, .size = real_size, .extends = false};
        block_init(region.addr,  (block_size){real_size}, NULL);
        return region;
    }

    struct region region = { .addr = region_ptr, .size = real_size, .extends = true};
    block_init(region.addr,  (block_size){real_size}, NULL);
    return region;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term( ) {
    void* start_region = HEAP_START;
    struct block_header* now_block = start_region;
    size_t now_region_size = 0;
    while (now_block != NULL){
        //Если блок сотрется, то нам нужно знать куда идти дальше
        struct block_header* temp_header = now_block->next;
        now_region_size += size_from_capacity(now_block->capacity).bytes;
        if(block_after(now_block) != now_block->next){
            munmap(start_region, now_region_size);
            start_region = temp_header;
            now_region_size = 0;
        } else if (temp_header == NULL){
            munmap(start_region, now_region_size);
            break;
        }
        now_block = temp_header;
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    size_t real_query = size_max(query, BLOCK_MIN_CAPACITY);
    if(!block_splittable(block, real_query)) {
        return false;
    }
    struct block_header* new = (struct block_header*) ((void *) block + offsetof(struct block_header, contents) + real_query);
    new->is_free = true;
    new->next = block->next;
    block->next = new;
    new->capacity.bytes = block->capacity.bytes - real_query - offsetof(struct block_header, contents);
    block->capacity.bytes = real_query;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    struct block_header* next_block = block_after(block);

    if (block->next == NULL || block->next != next_block || !mergeable(block, next_block)){
        return false;
    }
    block->next = next_block->next;
    block->capacity.bytes += next_block->capacity.bytes + offsetof(struct block_header, contents);
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static void link_all_blocks(struct block_header* block){
    struct block_header* now_block = block;
    while(now_block->next != NULL){
        if(try_merge_with_next(now_block)){
            continue;
        }
        now_block = now_block->next;
    }
}

static struct block_search_result find_good_or_last ( struct block_header* restrict block, size_t sz )    {
    link_all_blocks(block);
    struct block_header* now_block = block;
    while(now_block->next != NULL){
        if(now_block->is_free == true && now_block->capacity.bytes >= sz){
            return (struct block_search_result) {.block = now_block, .type = BSR_FOUND_GOOD_BLOCK};
        }
        now_block = now_block->next;
    }

    return (struct block_search_result){.block = now_block, .type = (now_block->capacity.bytes >= sz && now_block->is_free == true) ? BSR_FOUND_GOOD_BLOCK : BSR_REACHED_END_NOT_FOUND};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result result = find_good_or_last(block, query);
    if (result.type != BSR_FOUND_GOOD_BLOCK){
        return result;
    }
    split_if_too_big(result.block, query);
    result.block->is_free = false;
    return result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    void* region_end = block_after(last);
    struct region region = alloc_region(region_end, query);
    if(region.addr == NULL){
        return NULL;
    }
    last->next = region.addr;
    if (region.addr != region_end){
        return region.addr;
    }
    if(try_merge_with_next(last)){
        return last;
    }
    return region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    if(heap_start == NULL){
        struct region region = alloc_region(heap_start, query);
        if(region_is_invalid(&region)){
            return NULL;
        }
        heap_start = region.addr;
    }
    struct block_search_result maybe_result = try_memalloc_existing(query, heap_start);

    if (maybe_result.type == BSR_FOUND_GOOD_BLOCK){
        return maybe_result.block;
    }
    if(maybe_result.block == NULL){
        return NULL;
    }
    struct block_header* new = grow_heap(maybe_result.block, query);
    if (new != NULL){
        //Теперь мы точно расширили регион на память, которой точно хватит
        return try_memalloc_existing(query, new).block;
    }

    return NULL;
}

void* _malloc( size_t query ) {
      struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
      if (addr) return addr->contents;
      else return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
//Просто линкуем все свободные блоки после данного
void _free( void* mem ) {
    if (!mem) return ;
    struct block_header* header = block_get_header( mem );
    header->is_free = true;
    link_all_blocks(header);
}
