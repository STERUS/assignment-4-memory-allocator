#include "mem.h"
#include <stdio.h>
#define INIT_SIZE 0
#define REGION_SIZE 8192

struct test_structure{
    int64_t x;
    int64_t y;
};


int main(){
    debug("Start Debugging our Malloc\n");
    debug("Init Heap for 8192 bytes\n");

    void* heap = heap_init(INIT_SIZE);
    debug_heap(stderr, heap);

    debug("Try to allocate place for structure with size: %zu bytes\n", sizeof (struct test_structure));

    struct test_structure* element = _malloc(sizeof(struct test_structure));
    element->x = 3;
    element->y = 4;
    debug_heap(stderr, heap);
    fprintf(stderr, "Element is: x = %ld, y = %ld\n", element->x, element->y);

    debug("Try to allocate MANY OF MEMORY\n");

    void* some_pointer1 = _malloc(REGION_SIZE / 8);
    debug_heap(stderr, heap);

    debug("MUCH MORE\n");

    void* some_pointer2 = _malloc(REGION_SIZE / 2);
    debug_heap(stderr, heap);

    void* some_pointer3 = _malloc(REGION_SIZE / 2);
    debug_heap(stderr, heap);

    debug("Additional memory has been allocated, try to free some blocks\n");

    _free(some_pointer2);
    debug_heap(stderr,heap);
    debug("Free near Block to see merge\n");
    _free(some_pointer1);
    debug_heap(stderr, heap);

    debug("Try to allocate MANY MANY MEMORY\n");

    void* some_pointer4 = _malloc(REGION_SIZE * 3);
    debug_heap(stderr, heap);

    _free(some_pointer3);
    _free(some_pointer4);
    struct test_structure* another = _malloc(sizeof(struct test_structure));
    debug_heap(stderr, heap);

    _free(some_pointer1);
    _free(some_pointer2);
    _free(some_pointer3);
    _free(some_pointer4);
    _free(another);
    debug("Terminate heap\n");
    heap_term();

    debug("Try to see other addresses (mmap some memory after  region)\n");
    void* second = heap_init(INIT_SIZE);
    void* new = mmap(second + REGION_SIZE, REGION_SIZE * 3, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20 | 0x100000, -1, 0); //Ругается на Map NOREPLACE
    debug("%p - is a new mapped region after second heap\n", new);
    void* some_pointer7 = _malloc(REGION_SIZE * 2);
    debug_heap(stderr, second);
    _free(some_pointer7);
    debug("Terminate heap\n");
    heap_term();
    return 0;
}
